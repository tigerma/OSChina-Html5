define(['text!tweet/comment-reply.html', "base/openapi", 'base/util', '../base/login/login'],
	function(viewTemplate, OpenAPI, Util, Login) {
		return Piece.View.extend({
			id: 'tweet_comment-reply',
			events: {
				"click .backBtn": "goBack",
				"click .reply": "commentReply"
			},
			goBack: function() {
				history.back();
			},
			commentReply: function() {
				Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
				} else {
					this.commentReply1();
				}
			},
			commentReply1: function() {
				var replyCont = $(".replyTextarea").val();
				replyCont = replyCont.replace(/(\s*$)/g, ""); //去掉右边空格
				var isNotice = $(".replyNotice").is(":checked");;
				if (replyCont === "" || replyCont === " ") {
					new Piece.Toast('请输入评论内容');
				} else {
					var me = this;
					var userToken = Piece.Store.loadObject("user_token");
					var accessToken = userToken.access_token;
					var tweetId = Util.request("tweetId");
					var commentAuthorId = Util.request("commentAuthorId");
					var isChecked = $('#replyNotice').is(":checked");;
					if (isChecked) {
						isChecked = 1;
					} else {
						isChecked = 0;
					}
					Util.Ajax(OpenAPI.comment_reply, "GET", {
						access_token: accessToken,
						id: tweetId,
						catalog: 3,
						content: replyCont,
						isPostToMyZone: isChecked,
						authorid: commentAuthorId,
						"dataType": "jsonp"
					}, 'json', function(data, textStatus, jqXHR) {
						if (data.error === "200") {
							//alert("发布成功");
							new Piece.Toast('评论成功');
							setTimeout(function() {
								history.back();
							}, 1500);
						} else {
							new Piece.Toast(data.error_description)
						}
					}, null, null);

				}
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				//write your business logic here :)

				var commentList = Piece.Store.loadObject("cube-list-tweet-reply-list");
				var comentId = Util.request("commentId");
				comentId = parseInt(comentId); //item.id为int类型 ，所以先将comentId转换成int类型
				var data = null;
				$.each(commentList, function(index, item) {
					if (item.id === comentId) {
						data = item;
					}
				});
				var commentReplyTemplate = $(this.el).find("#commentReplyTemplate").html();
				var commentReplyHtml = _.template(commentReplyTemplate, data);
				$("#mainTweet").html("");
				$("#mainTweet").append(commentReplyHtml);
			}
		}); //view define

	});