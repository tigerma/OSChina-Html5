define(['text!demo/login.html'],
	function(viewTemplate) {
		return Piece.View.extend({
			id: 'demo_login',
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);


				var list = this.component('flightstatus-list');
				list.setRequestParams({
					'arrport': '',
					'depport': 'depport',
					'curpage': 1,
					'pageSize': 8,
					'ioroutport': 'io',
					'timestamp': new Date().getTime()
				});

				return this;
			},
			onShow: function() {
				//write your business logic here :)

				// $.ajax({
				// 	timeout: 2000 * 1000,
				// 	url: "http://www.oschina.net/action/oauth2/tweet",
				// 	type: "POST",
				// 	data: {
				// 		"access_token": 'db41bfb7-97fa-4850-bbd8-d98dde4fce53',
				// 		"msg": '今天中午不睡觉'
				// 	},
				// 	dataType: "json",
				// 	success: function(data, textStatus, jqXHR) {
				// 		console.info("=======success=======");
				// 		console.info(data);
				// 		console.info(textStatus);
				// 		console.info(jqXHR);
				// 	},
				// 	error: function(e, xhr, type) {
				// 		console.info("=======error=======");
				// 		console.info(e);
				// 		console.info(xhr);
				// 		console.info(type);
				// 	}
				// });

			}
		}); //view define

	});