define(['text!project/domestic-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'project-domestic-list',
			events: {
				"click .backBtn": "goBack",
				"click .projectList": "goToSoftDetail",
			},
			goBack: function() {
				var lastPage = Piece.Session.loadObject("osLastPage");
				this.navigateModule(lastPage, {
					trigger: true
				});
			},
			goToSoftDetail: function(el) {
				var $target = $(el.currentTarget);
				var url = $target.attr("data-url");
				this.navigate("software-detail?url=" + url, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				Util.loadList(this, 'project-domestic-list', OpenAPI.project_list, {
					'type': 'cn',
					'dataType': OpenAPI.dataType
				});
				//write your business logic here :)
			}
		}); //view define

	});