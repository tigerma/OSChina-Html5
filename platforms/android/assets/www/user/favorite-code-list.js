define(['text!user/favorite-code-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'user-favorite-code-list',
			events: {
				"click .backBtn": "goBack",
				"click .favoriteList": "goToCodeDetail",
			},
			goBack: function() {
				this.navigate("user-info", {
					trigger: true
				});
			},
			goToCodeDetail: function(el) {
				var $target = $(el.currentTarget);
				var url = $target.attr("data-url");
				location.href = url;
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;
				Util.loadList(this, 'user-favorite-code-list', OpenAPI.favorite_list, {
					'type': 5,
					'page': 1,
					'pageSize': OpenAPI.pageSize,
					'access_token': access_token,
					'dataType': OpenAPI.dataType
				});
				//write your business logic here :)
			}
		}); //view define

	});