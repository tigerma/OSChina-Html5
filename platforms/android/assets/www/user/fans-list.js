define(['text!user/fans-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'user-fans-list',
			events: {
				"click .backBtn": "goBack",
				"click .friendsList": "goToSeeUser",
			},
			goBack: function() {
				this.navigate("user-info", {
					trigger: true
				});
			},
			goToSeeUser: function(el) {
				var $target = $(el.currentTarget);
				var fromAuthorId = $target.attr("data-id");
				var fromAuthor = $target.attr("data-name");
				this.navigateModule("common/common-seeUser?fromAuthor=" + fromAuthor + "&fromAuthorId=" + fromAuthorId, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var attentionNum = Util.request("attentionNum");
				var fansNum = Util.request("fansNum");
				$("#attentionNum").html("");
				$("#fansNum").html("");
				$("#attentionNum").html(attentionNum);
				$("#fansNum").html(fansNum);
				$($("#io").find("li")[0]).attr({
					"data-value": "user/attention-list?attentionNum=" + attentionNum + "&fansNum=" + fansNum
				});
				$($("#io").find("li")[1]).attr({
					"data-value": "user/fans-list?attentionNum=" + attentionNum + "&fansNum=" + fansNum
				});
				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;
				Util.loadList(this, 'user-fans-list', OpenAPI.friends_list, {
					'relation': 0,
					'page': 1,
					'pageSize': OpenAPI.pageSize,
					'access_token': access_token,
					'dataType': OpenAPI.dataType
				});
				//write your business logic here :)
			}
		}); //view define

	});